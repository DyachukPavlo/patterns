package devices;

public interface ApplianceControl {
    public void on();
    public void off();
}
