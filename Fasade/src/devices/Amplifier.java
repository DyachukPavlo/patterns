package devices;

public class Amplifier implements ApplianceControl {
    @Override
    public void on() {
        System.out.println("Amplifier turn on");
    }

    @Override
    public void off() {
        System.out.println("Amplifier turn off");
    }
    public void setDvd(DVD dvd){
        System.out.println("Set on the dvd disk");
    }
    public void setVolume(int level){
        System.out.println("Set volum level to " + level);
    }
}
