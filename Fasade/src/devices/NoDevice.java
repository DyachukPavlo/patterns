package devices;

public class NoDevice implements ApplianceControl {
    @Override
    public void on() {

    }

    @Override
    public void off() {

    }

    public NoDevice() {
    }
}
