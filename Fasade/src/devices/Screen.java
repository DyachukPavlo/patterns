package devices;

public class Screen implements ApplianceControl{
    @Override
    public void on() {
        System.out.println("Screen on");
    }

    @Override
    public void off() {
        System.out.println("Screen off");
    }
}
