package devices;

public class DVD implements ApplianceControl {
    @Override
    public void on() {
        System.out.println("DVD turn on");
    }

    @Override
    public void off() {
        System.out.println("DVD turn off");
    }

    public void play(String movie){
        System.out.println("Play movie: " + movie);
    }
    public void stop(){
        System.out.println("Stop playing");
    }
}
