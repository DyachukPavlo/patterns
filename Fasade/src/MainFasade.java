import devices.*;
import theater.HomeTheaterFasade;

import java.util.stream.Stream;

public class MainFasade {
    public static void main(String[] args) {
        HomeTheaterFasade homeTheater = new HomeTheaterFasade(new TV(), new Stereo(), new CDPlayer(), new Screen(), new Light(), new Amplifier(), new DVD());
        homeTheater.watchMovie("Die hard");
        System.out.println("--------------------");
        homeTheater.endMovie();
    }
}
