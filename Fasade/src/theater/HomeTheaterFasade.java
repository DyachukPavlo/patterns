package theater;

import devices.*;

import java.util.stream.Stream;

public class HomeTheaterFasade {
    private TV tv;
    private Stereo stereo;
    private CDPlayer cdPlayer;
    private Screen screen;
    private Light light;
    private Amplifier amplifier;
    private DVD dvd;

    public HomeTheaterFasade(TV tv, Stereo stereo, CDPlayer cdPlayer, Screen screen, Light light, Amplifier amplifier, DVD dvd) {
        this.tv = tv;
        this.stereo = stereo;
        this.cdPlayer = cdPlayer;
        this.screen = screen;
        this.light = light;
        this.amplifier = amplifier;
        this.dvd = dvd;
    }

    public void watchMovie(String movie){
        System.out.println("Get ready to watch a movie...");
        light.off();
        screen.on();
        amplifier.on();
        amplifier.setDvd(dvd);
        amplifier.setVolume(5);
        dvd.on();
        dvd.play(movie);
    }

    public void endMovie(){
        System.out.println("Shutting movie theater down...");
        light.on();
        screen.off();
        amplifier.off();
        dvd.stop();
        dvd.off();
    }
}
