package embadded_realization;

import embadded_realization.data.WeatherData;
import embadded_realization.observers.HumidityObs;
import embadded_realization.observers.TemperatureObs;

import java.util.Observable;
import java.util.Observer;

public class ObserverMain {
    public static void main(String[] args) {
        Observer temp1 = new TemperatureObs("temp1");
        Observer hum = new HumidityObs("hum1");
        Observer temp2 = new TemperatureObs("temp2");

        WeatherData weatherData = new WeatherData();
        weatherData.addObserver(temp1);
        weatherData.addObserver(temp2);
        weatherData.addObserver(hum);

        weatherData.setTemperature(12.1);
        System.out.println("-------------------");

        weatherData.deleteObserver(temp1);

        weatherData.setHumidity(345.23);
    }
}
