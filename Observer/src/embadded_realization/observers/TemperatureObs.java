package embadded_realization.observers;


import embadded_realization.data.Measurements;
import embadded_realization.data.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class TemperatureObs implements Observer {
    private String name;

    public TemperatureObs(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


    @Override
    public int hashCode() {
        return super.hashCode() * 121;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData){
            System.out.println(name +": Temperature chenged. t = " + ((Measurements)arg).getTemperature() + " deg");
        }
    }
}
