package embadded_realization.observers;


import embadded_realization.data.Measurements;
import embadded_realization.data.WeatherData;

import java.util.Observable;
import java.util.Observer;

public class HumidityObs implements Observer {
    private String name;

    public HumidityObs(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData){
            System.out.println(name + ": Humidity changed. h = " + ((Measurements)arg).getHumidity());
        }
    }
}
