package embadded_realization.data;

public class Measurements {
    private double temperature;
    private double humidity;
    private double preasure;

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getPreasure() {
        return preasure;
    }

    public void setPreasure(double preasure) {
        this.preasure = preasure;
    }
}
