package embadded_realization.data;

import java.util.Observable;

public class WeatherData extends Observable{

    private Measurements measurements;

    public WeatherData() {
        this.measurements = new Measurements();
    }

    public void measurementsChanged(){
        setChanged();
        notifyObservers(measurements);
    }

    public void setTemperature(double temperature) {
        measurements.setTemperature(temperature);
        measurementsChanged();
    }

    public void setHumidity(double humidity) {
        measurements.setHumidity(humidity);
        measurementsChanged();
    }

    public void setPreasure(double preasure) {
        measurements.setPreasure(preasure);
        measurementsChanged();

    }

    public Measurements getMeasurements() {
        return measurements;
    }

    public void setMeasurements(Measurements measurements) {
        this.measurements = measurements;
        measurementsChanged();
    }

}
