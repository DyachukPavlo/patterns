package my_realization.observers;

import my_realization.data.Measurements;

public interface Observer {
    void update(Measurements data);
}
