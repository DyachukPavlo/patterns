package my_realization.observers;

import my_realization.data.Measurements;

public class HumidityObs implements Observer {
    private String name;

    public HumidityObs(String name) {
        this.name = name;
    }

    @Override
    public void update(Measurements data) {
        System.out.println(name + ": Humidity changed. h = " + data.getHumidity());
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
