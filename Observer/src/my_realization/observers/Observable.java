package my_realization.observers;

import my_realization.data.Measurements;

import java.util.HashSet;
import java.util.Set;

public class Observable {
    private Set<Observer> observers;

    public Observable() {
        observers = new HashSet<>();
    }

    public void register(Observer observer){
        observers.add(observer);
    }

    public void remove(Observer observer){
        observers.remove(observer);
    }

    public void notify(Measurements data){
        observers.stream().forEach(observer -> observer.update(data));
    }
}
