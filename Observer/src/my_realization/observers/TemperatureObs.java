package my_realization.observers;

import my_realization.data.Measurements;

public class TemperatureObs implements Observer {
    private String name;
    @Override
    public void update(Measurements data) {
        System.out.println(name +": Temperature chenged. t = " + data.getTemperature() + " deg");
    }

    public TemperatureObs(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }


    @Override
    public int hashCode() {
        return super.hashCode() * 121;
    }
}
