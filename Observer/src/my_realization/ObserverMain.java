package my_realization;

import my_realization.data.WeatherData;
import my_realization.observers.HumidityObs;
import my_realization.observers.Observable;
import my_realization.observers.Observer;
import my_realization.observers.TemperatureObs;

public class ObserverMain {
    public static void main(String[] args) {
        Observer temp1 = new TemperatureObs("temp1");
        Observer hum = new HumidityObs("hum1");
        Observer temp2 = new TemperatureObs("temp2");

        Observable obs = new Observable();
        obs.register(temp1);
        obs.register(temp2);
        obs.register(hum);

        WeatherData weatherData = new WeatherData();
        weatherData.setObservable(obs);

        weatherData.setTemperature(12.1);
        System.out.println("-------------------");

        obs.remove(temp1);

        weatherData.setHumidity(345.23);
    }
}
