package my_realization.data;

import my_realization.observers.Observable;

public class WeatherData {

    private Observable observable;
    private Measurements measurements;

    public WeatherData() {
        this.measurements = new Measurements();
    }

    public void measurementsChanged(){
        if (observable != null){
            observable.notify(measurements);
        }
    }

    public void setTemperature(double temperature) {
        measurements.setTemperature(temperature);
        measurementsChanged();
    }

    public void setHumidity(double humidity) {
        measurements.setHumidity(humidity);
        measurementsChanged();
    }

    public void setPreasure(double preasure) {
        measurements.setPreasure(preasure);
        measurementsChanged();

    }

    public Observable getObservable() {
        return observable;
    }

    public void setObservable(Observable observable) {
        this.observable = observable;
    }

    public Measurements getMeasurements() {
        return measurements;
    }

    public void setMeasurements(Measurements measurements) {
        this.measurements = measurements;
        measurementsChanged();
    }

}
