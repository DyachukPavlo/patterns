public class ChocolateBoiler {
    private static volatile ChocolateBoiler chocolateBoiler;
    private boolean empty;
    private boolean boiled;

    private ChocolateBoiler() {
        empty = true;
        boiled = false;
        System.out.println("The boiler was created");
    }

    public void fill(){
        if (isEmpty()){
            empty = false;
            boiled = false;
            System.out.println("The boiler is filled");
        }
    }

    public void boil(){
        if (!isEmpty() & !isBoiled()){
            boiled = true;
            System.out.println("the boiler is boiled");
        }
    }

    public void drain(){
        if (!isEmpty() & isBoiled()){
            empty = true;
            System.out.println("the boiler is drained");
        }
    }

    public boolean isEmpty() {
        return empty;
    }

    public boolean isBoiled() {
        return boiled;
    }

    public static ChocolateBoiler getInstance() {
        if (chocolateBoiler == null){
            synchronized (Singleton.class){
                if (chocolateBoiler == null) {
                    chocolateBoiler = new ChocolateBoiler();
                }
            }
        }
        return chocolateBoiler;
    }
}
