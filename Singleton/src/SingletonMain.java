public class SingletonMain {
    public static void main(String[] args) {

        Thread thread2 = new Thread(()-> {
            ChocolateBoiler chocolateBoiler2 = ChocolateBoiler.getInstance();
            chocolateBoiler2.fill();
            System.out.println("th2");
        });

        thread2.start();
        ChocolateBoiler chocolateBoiler = ChocolateBoiler.getInstance();
        chocolateBoiler.fill();
        chocolateBoiler.boil();

    }
}
