package birds;

public class WildTurkey implements Turkey {
    @Override
    public void gobble() {
        System.out.println("Wild turkey gobble");
    }

    @Override
    public void shortFly() {
        System.out.println("Wild turkey tries to fly");
    }
}
