package adapters;

import birds.Duck;
import birds.Turkey;

public class DuckAdapter implements Turkey {
    private Duck duck;

    public DuckAdapter(Duck duck) {
        this.duck = duck;
    }

    @Override
    public void gobble() {
        duck.quack();
    }

    @Override
    public void shortFly() {
        for (int i = 0; i<3; i++){
            duck.fly();
        }
    }
}
