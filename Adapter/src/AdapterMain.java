import adapters.TurkeyAdapter;
import birds.Duck;
import birds.MallardDuck;
import birds.Turkey;
import birds.WildTurkey;

public class AdapterMain {
    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();
        Turkey wildTurkey = new WildTurkey();
        Duck turkeyAdapter = new TurkeyAdapter(wildTurkey);

        mallardDuck.quack();
        mallardDuck.fly();

        turkeyAdapter.quack();
        turkeyAdapter.fly();
    }
}
