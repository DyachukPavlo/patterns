import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.List;

public class Menu implements MenuComponent {
    private List<MenuComponent> menuComponents;
    private String name;

    public Menu(String name) {
        this.name = name;
        this.menuComponents = new ArrayList<>();
    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public String getDescription() throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    @Override
    public double getPrice() throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    @Override
    public boolean isVegetarian() throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    @Override
    public void add(MenuComponent component) {
        menuComponents.add(component);
    }

    @Override
    public void remove(MenuComponent component) {
        menuComponents.remove(component);
    }

    @Override
    public MenuComponent getChild(int i) {
        return menuComponents.get(i);
    }

    public List<MenuComponent> getMenuComponents() {
        return menuComponents;
    }

    @Override
    public void print() {
        System.out.println(getName());
        System.out.println("-------------------------");
        menuComponents.forEach(MenuComponent::print);
    }
}
