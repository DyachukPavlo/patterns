import javax.naming.OperationNotSupportedException;
import java.util.Iterator;

public class PancakeHouseMenu {
    private MenuComponent menuComponents;

    public PancakeHouseMenu() {
        this.menuComponents = new Menu("This is Pancake main menu");
        addItem(new MenuItem("K&B`s Pancake Breakfast", "Pasasasasa", true, 2.99));
        addItem(new MenuItem("Regular Pancake Breakfast", "Hihijsodhjoisd", false, 4.15));
        addItem(new MenuItem("Blueberry", "yuj6tffe", false, 3.18));
        addItem(new MenuItem("Toasts", "ythrtg", true, 0.99));
        addItem(new MenuItem("Waffles", "retegreg", true, 3.49));

        MenuComponent desertMenu = new Menu("This is Pancake desert menu");
        try {
            desertMenu.add(new MenuItem("K&B`s desert", "desert 1", true, 19));
            desertMenu.add(new MenuItem("Ice cream", "ice", true, 2));
            desertMenu.add(new MenuItem("Chocolate", "choco", true, 0.58));
        } catch (OperationNotSupportedException e) {
            e.printStackTrace();
        }
        addItem(desertMenu);
    }

    public void addItem(MenuComponent menuComponent){
        try {
            menuComponents.add(menuComponent);
        } catch (OperationNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public MenuComponent getMenuComponents() {
        return menuComponents;
    }
}
