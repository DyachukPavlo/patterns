import javax.naming.OperationNotSupportedException;

public interface MenuComponent {
    String getName() throws OperationNotSupportedException;
    String getDescription() throws OperationNotSupportedException;
    double getPrice() throws OperationNotSupportedException;
    boolean isVegetarian() throws OperationNotSupportedException;
    void add(MenuComponent component) throws OperationNotSupportedException;
    void remove(MenuComponent component) throws OperationNotSupportedException;
    MenuComponent getChild(int i) throws OperationNotSupportedException;
    void print();
}
