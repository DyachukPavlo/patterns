import javax.naming.OperationNotSupportedException;

public class CompositeMain {
    public static void main(String[] args) throws OperationNotSupportedException {
        MenuComponent supermenu = new Menu("This is supermenu");

        supermenu.add(new PancakeHouseMenu().getMenuComponents());
        supermenu.print();
    }
}
