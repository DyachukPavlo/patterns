package controller;

import commands.Command;

public class StereoSlot implements Slot {
    private Command button1;
    private Command button2;

    public StereoSlot(Command button1, Command button2) {
        this.button1 = button1;
        this.button2 = button2;
    }

    public Command getButton1() {
        return button1;
    }

    public void setButton1(Command button1) {
        this.button1 = button1;
    }

    public Command getButton2() {
        return button2;
    }

    public void setButton2(Command button2) {
        this.button2 = button2;
    }
}
