package controller;

import commands.Command;

public interface Slot {
    public Command getButton1();
    public void setButton1(Command command);
    public Command getButton2();
    public void setButton2(Command command);

}
