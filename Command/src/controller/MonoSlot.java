package controller;

import commands.Command;
import commands.OffCommand;
import commands.OnCommand;
import devices.ApplianceControl;

public class MonoSlot implements Slot{
    private ApplianceControl device;
    private Command button1;
    private Command button2;

    public MonoSlot(ApplianceControl device) {
        this.device = device;
        this.button1 = new OnCommand(device);
        this.button2 = new OffCommand(device);
    }

    public ApplianceControl getDevice() {
        return device;
    }

    public void setDevice(ApplianceControl device) {
        this.device = device;
    }

    public Command getButton1() {
        return button1;
    }

    public void setButton1(Command command) {
        this.button1 = command;
    }

    public Command getButton2() {
        return button2;
    }

    public void setButton2(Command command) {
        this.button2 = command;
    }
}
