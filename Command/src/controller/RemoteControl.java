package controller;

import commands.Command;
import commands.NoCommand;
import devices.NoDevice;

import java.util.ArrayList;
import java.util.List;

public class RemoteControl {
    private List<Slot> slots;
    private Command undoCommand;

    public RemoteControl() {
        this.slots = new ArrayList<>(7);
        undoCommand = new NoCommand();
    }

    public void setSlot(Slot slot, int slotNumber){
        slots.add(slotNumber, slot);
    }

    public void buttonOnPressed(int slotNumber){
        Slot currSlot = slots.get(slotNumber);
        currSlot.getButton1().execute();
        undoCommand = currSlot.getButton1();
    }
    public void buttonOffPressed(int slotNumber){
        Slot currSlot = slots.get(slotNumber);
        slots.get(slotNumber).getButton2().execute();
        undoCommand = currSlot.getButton2();
    }

    public void buttonUndoPressed(){
        System.out.println("Undo pressed:");
        undoCommand.undo();
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }
}
