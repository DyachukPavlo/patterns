package commands;

import devices.ApplianceControl;
import devices.Light;

public class OffCommand implements Command {
    private ApplianceControl device;

    public OffCommand(ApplianceControl device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.off();
    }

    @Override
    public void undo() {
        device.on();
    }

}
