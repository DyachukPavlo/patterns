package commands;

import devices.ApplianceControl;
import devices.Light;

public class OnCommand implements Command {
    private ApplianceControl device;

    public OnCommand(ApplianceControl device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.on();
    }

    @Override
    public void undo() {
        device.off();
    }
}
