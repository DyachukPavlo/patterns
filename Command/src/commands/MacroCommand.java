package commands;

import java.util.List;

public class MacroCommand implements Command {
    List<Command> commands;

    public MacroCommand(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        commands.forEach(command -> command.execute());
    }

    @Override
    public void undo() {
        commands.forEach(command -> command.undo());
    }
}
