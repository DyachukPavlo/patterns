import commands.*;
import controller.RemoteControl;
import controller.MonoSlot;
import controller.Slot;
import controller.StereoSlot;
import devices.*;

import java.util.ArrayList;
import java.util.List;

public class CommandMain {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();
        Slot noCommandSlot = new MonoSlot(new NoDevice());
        for (int i = 0; i<7; i++){
            remoteControl.setSlot(noCommandSlot, i);
        }

        Slot s0 = new MonoSlot(new Light());
        remoteControl.setSlot(s0, 0);
        remoteControl.buttonOnPressed(0);

        remoteControl.buttonUndoPressed();
        remoteControl.buttonUndoPressed();

        ApplianceControl stereo = new Stereo();
        Command onStereo = new OnCommand(stereo);
        Command offStereo = new OffCommand(stereo);

        ApplianceControl tv = new TV();
        Command onTV = new OnCommand(tv);
        Command offTV = new OffCommand(tv);

        List<Command> onCommands = new ArrayList<>();
        onCommands.add(onStereo);
        onCommands.add(onTV);

        List<Command> offCommands = new ArrayList<>();
        offCommands.add(offStereo);
        offCommands.add(offTV);

        Command macroOn = new MacroCommand(onCommands);
        Command macroOff = new MacroCommand(offCommands);

        remoteControl.setSlot(new StereoSlot(macroOn, macroOff), 2);
        remoteControl.buttonOnPressed(2);
        remoteControl.buttonUndoPressed();
    }
}
