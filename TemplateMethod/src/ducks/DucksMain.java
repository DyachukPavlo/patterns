package ducks;

import java.util.Arrays;

public class DucksMain {
    public static void main(String[] args) {
        Duck [] ducks = {
                new Duck("Daffy", 8),
                new Duck("Donald", 10),
                new Duck("Alfred", 3),
                new Duck("Arnold", 12)
        };
        System.out.println("Before sorting: ");
        display(ducks);
        Arrays.sort(ducks);
        System.out.println("\nAfter sorting:");
        display(ducks);
    }
    private static void display(Duck[] ducks){
        Arrays.asList(ducks).forEach(System.out::println);
    }
}
