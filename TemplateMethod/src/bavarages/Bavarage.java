package bavarages;

public abstract class Bavarage {
    public final void prepareRecipe(){
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()){
            addCondiments();
        }
    }
    public void boilWater(){
        System.out.println("Boiling water");
    }
    public void pourInCup(){
        System.out.println("Pouring into cup");
    }
    public abstract void brew();
    public abstract void addCondiments();
    boolean customerWantsCondiments(){
        return true;
    }
}
