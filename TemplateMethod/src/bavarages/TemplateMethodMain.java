package bavarages;

public class TemplateMethodMain {
    public static void main(String[] args) {
        Bavarage tea = new Tea();
        Bavarage coffee = new Coffee();

        tea.prepareRecipe();
        coffee.prepareRecipe();
    }
}
