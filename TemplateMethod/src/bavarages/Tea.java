package bavarages;

import bavarages.Bavarage;

public class Tea extends Bavarage {
    @Override
    public void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    public void addCondiments() {
        System.out.println("Adding Lemon");
    }
}
