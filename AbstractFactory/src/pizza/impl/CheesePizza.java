package pizza.impl;

import ingridient_factories.factories.IngridientFactory;
import ingridient_factories.factories.impl.MafiaIngridientFactory;
import ingridient_factories.ingridients.Ingridient;
import pizza.Pizza;

import java.util.ArrayList;
import java.util.List;

public class CheesePizza extends Pizza {

    private final IngridientFactory ingridientFactory;

    public CheesePizza(IngridientFactory ingridientFactory, String pizzaStoreName) {
        this.ingridientFactory = ingridientFactory;
        setName(pizzaStoreName + " cheese pizza");
    }

    @Override
    public void prepare() {
        setSauce(ingridientFactory.createSauce());
        List<Ingridient> toppings = new ArrayList();
        toppings.add(ingridientFactory.createCheese());
        toppings.add(ingridientFactory.createMeat());
        setToppings(toppings);

        System.out.println("preparing " + getName());
        System.out.println("adding sauce " + getSauce().getName());
        System.out.println("adding toppings: ");
        toppings.forEach(topping ->{
            System.out.println("-" + topping.getName() + " ");
        });
    }
}
