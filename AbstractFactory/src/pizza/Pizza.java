package pizza;

import ingridient_factories.ingridients.Ingridient;
import ingridient_factories.ingridients.Sauce;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza{
    private String name;
    private Sauce sauce;
    private List<Ingridient> toppings = new ArrayList();

    public abstract void prepare();
    public void bake(){
        System.out.println("bake for 25 mins at 350 degrees");
    }
    public void cut(){
        System.out.println("cutting into slices");
    }
    public void box(){
        System.out.println("place pizza into official box");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public List getToppings() {
        return toppings;
    }

    public void setToppings(List toppings) {
        this.toppings = toppings;
    }
}
