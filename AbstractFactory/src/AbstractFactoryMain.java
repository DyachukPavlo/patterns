import pizzastore.MafiaPizzaStore;
import pizzastore.MamamiaPizzaStore;
import pizzastore.PizzaStore;

public class AbstractFactoryMain {
    public static void main(String[] args) {
        PizzaStore mamamia = new MamamiaPizzaStore();
        PizzaStore mafia = new MafiaPizzaStore();

        mamamia.orderPizza("cheese");
        mafia.orderPizza("cheese");

        mamamia.orderPizza("meat");
    }
}
