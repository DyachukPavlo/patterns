package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Meat;

public class Chicken extends Meat {
    public Chicken() {
        setName("chicken");
    }
}
