package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Cheese;

public class Chedder extends Cheese {
    public Chedder() {
        setName("chedder");
    }
}
