package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Sauce;

public class Marinara extends Sauce {
    public Marinara() {
        setName("marinara");
    }
}
