package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Sauce;

public class Chumak extends Sauce {
    public Chumak() {
        setName("chumak");
    }
}
