package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Cheese;

public class Parmezan extends Cheese {
    public Parmezan() {
        setName("parmezan");
    }
}
