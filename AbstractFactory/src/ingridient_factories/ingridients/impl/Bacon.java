package ingridient_factories.ingridients.impl;

import ingridient_factories.ingridients.Meat;

public class Bacon extends Meat {
    public Bacon() {
        setName("bacon");
    }
}
