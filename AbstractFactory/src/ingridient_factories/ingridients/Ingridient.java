package ingridient_factories.ingridients;

public abstract class Ingridient {
    private String name;

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }
}
