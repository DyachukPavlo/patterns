package ingridient_factories.factories;

import ingridient_factories.ingridients.Cheese;
import ingridient_factories.ingridients.Meat;
import ingridient_factories.ingridients.Sauce;

public interface IngridientFactory {
    public Cheese createCheese();
    public Meat createMeat();
    public Sauce createSauce();
}
