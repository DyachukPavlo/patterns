package ingridient_factories.factories.impl;

import ingridient_factories.factories.IngridientFactory;
import ingridient_factories.ingridients.Cheese;
import ingridient_factories.ingridients.Meat;
import ingridient_factories.ingridients.Sauce;
import ingridient_factories.ingridients.impl.Chedder;
import ingridient_factories.ingridients.impl.Chicken;
import ingridient_factories.ingridients.impl.Marinara;

public class MamamiaIngridientFactory implements IngridientFactory {
    @Override
    public Cheese createCheese() {
        return new Chedder();
    }

    @Override
    public Meat createMeat() {
        return new Chicken();
    }

    @Override
    public Sauce createSauce() {
        return new Marinara();
    }
}
