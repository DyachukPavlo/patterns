package ingridient_factories.factories.impl;

import ingridient_factories.factories.IngridientFactory;
import ingridient_factories.ingridients.Cheese;
import ingridient_factories.ingridients.Meat;
import ingridient_factories.ingridients.Sauce;
import ingridient_factories.ingridients.impl.Bacon;
import ingridient_factories.ingridients.impl.Chumak;
import ingridient_factories.ingridients.impl.Parmezan;

public class MafiaIngridientFactory implements IngridientFactory {
    @Override
    public Cheese createCheese() {
        return new Parmezan();
    }

    @Override
    public Meat createMeat() {
        return new Bacon();
    }

    @Override
    public Sauce createSauce() {
        return new Chumak();
    }
}
