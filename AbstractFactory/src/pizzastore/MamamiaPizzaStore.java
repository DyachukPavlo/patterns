package pizzastore;

import pizza_factories.MamamiaPizzaFactory;

public class MamamiaPizzaStore extends PizzaStore {
    public MamamiaPizzaStore() {
        super(new MamamiaPizzaFactory());
    }

}
