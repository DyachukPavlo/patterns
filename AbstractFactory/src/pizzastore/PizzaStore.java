package pizzastore;

import pizza_factories.PizzaFactory;
import pizza.Pizza;

public class PizzaStore {
    private PizzaFactory pizzaFactory;

    protected PizzaStore(PizzaFactory pizzaFactory) {
        this.pizzaFactory = pizzaFactory;
    }

    public void orderPizza(String type){
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
    }
    private Pizza createPizza(String type){
        return type.equals("cheese") ? pizzaFactory.prepareCheesePizza() : pizzaFactory.prepareMeatPizza();
    }
}
