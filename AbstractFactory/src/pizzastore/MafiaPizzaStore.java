package pizzastore;

import pizza_factories.MafiaPizzaFactory;

public class MafiaPizzaStore extends PizzaStore {
    public MafiaPizzaStore() {
        super(new MafiaPizzaFactory());
    }
}
