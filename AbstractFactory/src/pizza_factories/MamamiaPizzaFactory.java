package pizza_factories;

import ingridient_factories.factories.IngridientFactory;
import ingridient_factories.factories.impl.MamamiaIngridientFactory;
import pizza.Pizza;
import pizza.impl.CheesePizza;
import pizza.impl.MeatPizza;

public class MamamiaPizzaFactory implements PizzaFactory {

    @Override
    public Pizza prepareCheesePizza() {
        return new CheesePizza(new MamamiaIngridientFactory(), "Mamamia");
    }

    @Override
    public Pizza prepareMeatPizza() {
        return new MeatPizza(new MamamiaIngridientFactory(), "Mamamia");
    }
}
