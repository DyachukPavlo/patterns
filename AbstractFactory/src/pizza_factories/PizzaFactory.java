package pizza_factories;

import pizza.Pizza;

public interface PizzaFactory {
    Pizza prepareCheesePizza();
    Pizza prepareMeatPizza();
}
