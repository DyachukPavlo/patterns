package pizza_factories;

import ingridient_factories.factories.IngridientFactory;
import ingridient_factories.factories.impl.MafiaIngridientFactory;
import pizza.Pizza;
import pizza.impl.CheesePizza;
import pizza.impl.MeatPizza;

public class MafiaPizzaFactory implements PizzaFactory{
    private final IngridientFactory ingridientFactory;

    public MafiaPizzaFactory() {
        this.ingridientFactory = new MafiaIngridientFactory();
    }

    @Override
    public Pizza prepareCheesePizza() {
        return new CheesePizza(ingridientFactory, "Mafia");
    }

    @Override
    public Pizza prepareMeatPizza() {
        return new MeatPizza(ingridientFactory, "Mafia");
    }
}
