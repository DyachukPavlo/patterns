package stream;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DecoratorMain {
    public static void main(String[] args) {
        int c;
        InputStream in = new LowerCaseDecorator(new BufferedInputStream(System.in));
        try {
            while ((c = in.read())>0){
                System.out.print((char)(c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
