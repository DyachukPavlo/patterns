package cafe.components;

public abstract class Beverage {
    private String description = "Unknown beverage";
    private Size size;

    public abstract double calcCost();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
