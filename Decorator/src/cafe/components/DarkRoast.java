package cafe.components;

public class DarkRoast extends Beverage {
    private double cost;

    public DarkRoast(Size size) {
        setSize(size);
        setDescription("dark roast");
        setCost(size == Size.SMALL ? 1.15 : size == Size.STANDARD ? 1.35 : 1.5);
    }

    @Override
    public double calcCost() {
        return cost;
    }

    public double getCost() {
        return cost;
    }

    private void setCost(double cost) {
        this.cost = cost;
    }
}
