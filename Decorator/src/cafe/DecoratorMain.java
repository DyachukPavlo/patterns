package cafe;

import cafe.components.Beverage;
import cafe.components.DarkRoast;
import cafe.components.Size;
import cafe.decorators.Milk;
import cafe.decorators.Mocha;

public class DecoratorMain {
    public static void main(String[] args) {
        Beverage darkRoast = new Mocha(new Milk(new DarkRoast(Size.STANDARD)));
        System.out.println(darkRoast.calcCost());
        System.out.println(darkRoast.getDescription());
    }
}
