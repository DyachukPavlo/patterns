package cafe.decorators;

import cafe.components.Beverage;
import cafe.components.Size;

public class Mocha extends Decorator {
    private Beverage beverage;
    private double cost;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
        setSize(beverage.getSize());
        Size size = getSize();
        cost = size == Size.SMALL ? 0.15 : size == Size.STANDARD ? 0.2 : 0.25;
    }

    @Override
    public double calcCost() {
        return beverage.calcCost() + cost;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", mocha";
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
