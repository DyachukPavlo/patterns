package cafe.decorators;

import cafe.components.Beverage;

public abstract class Decorator extends Beverage {
    public abstract String getDescription();

}
