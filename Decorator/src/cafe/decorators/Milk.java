package cafe.decorators;

import cafe.components.Beverage;
import cafe.components.Size;

public class Milk extends Decorator {
    private Beverage beverage;
    private double cost;

    public Milk(Beverage beverage) {
        this.beverage = beverage;
        setSize(beverage.getSize());
        Size size = getSize();
        cost = size == Size.SMALL ? 0.25 : size == Size.STANDARD ? 0.3 : 0.35;
    }

    @Override
    public double calcCost() {
        return  beverage.calcCost() + cost;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", milk";
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
