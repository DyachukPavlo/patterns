package ducks;

import behaviors.impl.FlyNoWay;
import behaviors.impl.Squeak;

public class RubberDuck extends Duck {
    public RubberDuck() {
        super(new FlyNoWay(), new Squeak());
    }

    @Override
    public void display() {
        System.out.println("This is rubber duck");
    }

}
