package ducks;

public class MallardDuck extends Duck {

    public MallardDuck() {
      //  super(new FlyWithWings(), new Quack());
    }

    @Override
    public void swim() {
        System.out.println("Mallard swim");
    }

    @Override
    public void display() {
        System.out.println("This is mallard");
    }

}
