import behaviors.impl.FlyWithWings;
import behaviors.impl.Quack;
import ducks.Duck;
import ducks.MallardDuck;
import ducks.RubberDuck;

import java.util.ArrayList;
import java.util.List;

public class StrategyMain {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        Duck rubber = new RubberDuck();

        List<Duck> ducks = new ArrayList<>();
        ducks.add(mallard);
        ducks.add(rubber);

        mallard.setFlyBehavior(new FlyWithWings());
        mallard.setQuackBehavior(new Quack());

        ducks.forEach(duck -> {
            duck.display();
            duck.swim();
            duck.performFly();
            duck.performQuack();
            System.out.println("-------------------------");
        });
    }
}
