import menu.DinerMenu;
import menu.Menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Waitress {
    private List<Menu> menuList;

    public Waitress(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Waitress(Menu... menu) {
        if (menuList == null){
            menuList = new ArrayList();
        }
        for (Menu item : menu){
            menuList.add(item);
        }
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public void printMenu(){
        menuList.forEach(item->printMenu(item.createIterator()));
    }

    private void printMenu(Iterator iterator){
        iterator.forEachRemaining(System.out::println);
    }
}
