import menu.DinerMenu;
import menu.PancakeHouseMenu;

public class IteratorMain {
    public static void main(String[] args) {
        Waitress waitress = new Waitress(new PancakeHouseMenu(), new DinerMenu());
        waitress.printMenu();
    }
}
