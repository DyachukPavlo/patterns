package menu;

import iterators.DinerMenuIterator;

import java.util.Iterator;

public class DinerMenu implements Menu{
    private static final int MAX_ITEMS = 6;
    private int numbersOfItems = 0;
    private MenuItem[] menuItems;

    public DinerMenu() {
        menuItems = new MenuItem[MAX_ITEMS];

        addItem(new MenuItem("Vegetarian BLT", "BLT veg", true, 2.99));
        addItem(new MenuItem("Soup of the day", "soup", false, 5.15));
        addItem(new MenuItem("Hot-Dog", "hot dog", false, 2.55));
    }

    public void addItem(MenuItem menuItem){
        if (numbersOfItems >= MAX_ITEMS){
            System.out.println("Sorry, menu is full!");
        }
        else {
            menuItems[numbersOfItems] = menuItem;
            numbersOfItems = numbersOfItems +1;
        }
    }

    public Iterator createIterator(){
        return new DinerMenuIterator(menuItems);
    }
}
