package menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PancakeHouseMenu implements Menu {
    private List menuItems;

    public PancakeHouseMenu() {
        this.menuItems = new ArrayList();
        addItem(new MenuItem("K&B`s Pancake Breakfast", "Pasasasasa", true, 2.99));
        addItem(new MenuItem("Regular Pancake Breakfast", "Hihijsodhjoisd", false, 4.15));
        addItem(new MenuItem("Blueberry", "yuj6tffe", false, 3.18));
        addItem(new MenuItem("Toasts", "ythrtg", true, 0.99));
        addItem(new MenuItem("Waffles", "retegreg", true, 3.49));
    }
    public void addItem(MenuItem menuItem){
        menuItems.add(menuItem);
    }

    public Iterator createIterator(){
        return menuItems.iterator();
    }
}
